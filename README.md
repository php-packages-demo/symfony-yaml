# symfony/[yaml](https://phppackages.org/p/symfony/yaml)

The Yaml component loads and dumps YAML files. https://symfony.com/yaml

Unofficial Howto and demo

[![PHPPackages Rank](http://phppackages.org/p/symfony/yaml/badge/rank.svg)](http://phppackages.org/p/symfony/yaml)
[![PHPPackages Referenced By](http://phppackages.org/p/symfony/yaml/badge/referenced-by.svg)](http://phppackages.org/p/symfony/yaml)

## Online YAML validators
* https://yamlvalidator.com/

## YAML documentation
* https://en.wikipedia.org/wiki/YAML
* [*Learn X in Y minutes, Where X=yaml*](https://learnxinyminutes.com/docs/yaml/)